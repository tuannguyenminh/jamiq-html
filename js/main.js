// create the module and name it scotchApp
	var scotchApp = angular.module('jamApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'tmp/home.html',
				controller  : 'mainController'
			})

			// route for the fb page
			.when('/fb', {
				templateUrl : 'tmp/fb.html',
				controller  : 'fbController'
			})
			// route for the post page
			.when('/post', {
				templateUrl : 'tmp/post.html',
				controller  : 'postController'
			})
			// route for the cat page
			.when('/cat', {
				templateUrl : 'tmp/cat.html',
				controller  : 'catController'
			})
			// route for the user page
			.when('/user', {
				templateUrl : 'tmp/user.html',
				controller  : 'userController'
			})// route for the user page

			.when('/createpost', {
				templateUrl : 'tmp/createpost.html',
				controller  : 'createpostController'
			})
			.when('/analytics', {
				templateUrl : 'tmp/analytics.html',
				controller  : 'analyticsController'
			}).when('/inf', {
				templateUrl : 'tmp/influence.html',
				controller  : 'influenceController'
			});



	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController',['$scope', '$location', function($scope,$location) {
		// create a message to display in our view
		$scope.page_title = 'DASHBOARD';
		
		$scope.getClass = function(path) {

		    if ($location.path().substr(0, path.length) == path) {
		      return "active";
		    } else {
		      return "";
		    }
		};

		$scope.openPanel = function(event){
			jQuery('.app-aside').toggleClass('off-screen');
		}

	}]);

	scotchApp.controller('fbController',function($scope) {
		$scope.page_title = 'FACEBOOK PAGE';

	});

	scotchApp.controller('postController', function($scope) {
		$scope.page_title = 'POST';


	});

	scotchApp.controller('catController', function($scope) {
		$scope.page_title = 'CATEGORY';
	});

	scotchApp.controller('userController', function($scope) {
		$scope.page_title = 'USERS';
	});

	scotchApp.controller('createpostController', function($scope) {
		$scope.page_title = 'POST CREATE';

		$scope.switchType = function(event){			
			jQuery("#postType li a").removeClass('active');
			jQuery(event.target).parent().addClass('active');
			var id = jQuery(event.target).parent().data('type');
			console.log(id);
			jQuery('.post-data').removeClass('active');
			jQuery('#postType'+id).addClass('active');
		};

	}).directive('customDatepicker',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){

            	jQuery.dpText = {				
					TEXT_CHOOSE_DATE:'', 
				};

               $element.datePicker();
               $element.dpSetOffset(30, -143);
            }    
        };
    }).directive('timepicker',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){
               $element.timepicker();
            }    
        };
    });

	scotchApp.controller('analyticsController', function($scope) {
		$scope.page_title = 'POST ANALYTICS';
	}).directive('datacharts',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){

               $element.highcharts({
			        chart: {
			            type: 'line'
			        },
			        title: {
			            text: 'Monthly Average Temperature'
			        },
			        subtitle: {
			            text: 'Source: WorldClimate.com'
			        },
			        xAxis: {
			            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			        },
			        yAxis: {
			            title: {
			                text: 'Temperature (°C)'
			            }
			        },
			        plotOptions: {
			            line: {
			                dataLabels: {
			                    enabled: true
			                },
			                enableMouseTracking: false
			            }
			        },
			        series: [{
			            name: 'Tokyo',
			            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
			        }, {
			            name: 'London',
			            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
			        }]
			    });



            }    
        };
    }).directive('datepicker',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){

            	jQuery.dpText = {				
					TEXT_CHOOSE_DATE:'', 
				};

               $element.datePicker();
               $element.dpSetOffset(30, -143);

               jQuery('#start-date').bind(
					'dpClosed',
					function(e, selectedDates)
					{
						var d = selectedDates[0];
						if (d) {
							d = new Date(d);
							jQuery('#end-date').dpSetStartDate(d.addDays(1).asString());
						}
					}
				);

				jQuery('#end-date').bind(
					'dpClosed',
					function(e, selectedDates)
					{
						var d = selectedDates[0];
						if (d) {
							d = new Date(d);
							jQuery('#start-date').dpSetEndDate(d.addDays(-1).asString());
						}
					}
				);
            }    
        };
    }).directive('datapie',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){

               $element.highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: 1,//null,
			            plotShadow: false
			        },
			        title: {
			            text: 'Browser market shares at a specific website, 2014'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            type: 'pie',
			            name: 'Browser share',
			            data: [
			                ['Firefox',   45.0],
			                ['IE',       26.8],
			                {
			                    name: 'Chrome',
			                    y: 12.8,
			                    sliced: true,
			                    selected: true
			                },
			                ['Safari',    8.5],
			                ['Opera',     6.2],
			                ['Others',   0.7]
			            ]
			        }]
			    });



            }    
        };
    });

	scotchApp.controller('influenceController', function($scope) {
		$scope.page_title = 'INFLUENCE';
	}).directive('datapie',function($compile){
        return {
            restrict:'A',
            link: function($scope, $element){

               $element.highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: 1,//null,
			            plotShadow: false
			        },
			        title: {
			            text: ''
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            type: 'pie',
			            name: 'Browser share',
			            data: [
			                ['Firefox',   45.0],
			                ['IE',       26.8],
			                {
			                    name: 'Chrome',
			                    y: 12.8,
			                    sliced: true,
			                    selected: true
			                },
			                ['Safari',    8.5],
			                ['Opera',     6.2],
			                ['Others',   0.7]
			            ]
			        }]
			    });



            }    
        };
    });
